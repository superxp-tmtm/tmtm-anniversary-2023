// XPTloadouts.hpp
// Used for defining advanced respawn loadouts for players
// Default behaviour is to check if the player unit has a special loadout defined. Otherwise, it will check to see if the classname matches a loadout
// Advanced functionality allows mission creator to define exactly where items are placed in unit inventories
// Also supports sub-loadout randomization. If a loadout has sub-classes defined, the script will automatically select one of them to apply to the unit.
class loadouts
{		
	// Empty loadout with comments removed. Use this for your loadouts
	class base
	{
		displayName = "Base Loadout";
		
		primaryWeapon[] = {"", "", "", "", {}, {}, ""};
		secondaryWeapon[] = {"", "", "", "", {}, {}, ""};
		handgunWeapon[] = {"", "", "", "", {}, {}, ""};
		binocular = "";
		
		uniformClass = "";
		headgearClass = "";
		facewearClass = "";
		vestClass = "";
		backpackClass = "";
		
		linkedItems[] = {"ItemMap", "ItemGPS", "ItemRadio", "ItemCompass", "ItemWatch", "NVGoggles"};
		
		uniformItems[] = {};
		vestItems[] = {};
		backpackItems[] = {};
		
		basicMedUniform[] = {};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		
		advMedUniform[] = {};
		advMedVest[] = {};
		advMedBackpack[] = {};
	};
	
	class B_Soldier_F: base {
		// Requires the following DLC:
		// Contact Platform
		displayName = "Yellow Plt Rifleman";

		primaryWeapon[] = {"CUP_CZ_BREN2_762_8_Grn","","CUP_acc_Flashlight","optic_Holosight_blk_F",{"CUP_30Rnd_TE1_Green_Tracer_762x39_CZ807",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"CUP_hgun_Glock17_blk","","CUP_acc_Glock17_Flashlight","",{"CUP_17Rnd_9x19_glock17",17},{},""};
		binocular = "Binocular";

		uniformClass = "tmtm_u_combatUniform_erdl";
		headgearClass = "tmtm_h_k6_ranger_visorUp";
		vestClass = "tmtm_v_modularVestHiVis_rangerYellow";

		linkedItems[] = {"ItemMap","ItemMicroDAGR","TFAR_anprc152","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"ACE_EntrenchingTool",1},{"ItemcTabHCam",1},{"CUP_17Rnd_9x19_glock17",2,17},{"SmokeShell",3,1}};
		vestItems[] = {{"ACE_CableTie",3},{"CUP_30Rnd_TE1_Green_Tracer_762x39_CZ807",8,30},{"MiniGrenade",2,1}};

		basicMedUniform[] = {{"ACE_fieldDressing",10},{"ACE_epinephrine",2},{"ACE_morphine",2}};
		basicMedVest[] = {{"ACE_bloodIV_500",1}};
	};
	
	class B_Soldier_TL_F: B_Soldier_F {};
	
	class B_Soldier_AR_F: B_Soldier_F {
		// Requires the following DLC:
		// Western Sahara
		// Contact Platform
		displayName = "Yellow Plt Autorifleman";

		primaryWeapon[] = {"LMG_S77_lxWS","","saber_light_lxWS","",{"100Rnd_762x51_S77_Green_lxWS",100},{},""};

		vestItems[] = {{"ACE_CableTie",3},{"MiniGrenade",2,1},{"100Rnd_762x51_S77_Green_Tracer_lxWS",3,100}};
	};
	
	class B_Soldier_AAR_F: B_Soldier_F {
		// Requires the following DLC:
		// Western Sahara
		// Contact Platform
		displayName = "Yellow Plt Assistant Autorifleman";
		
		backpackClass = "B_AssaultPack_sgg";
		backpackItems[] = {{"100Rnd_762x51_S77_Green_Tracer_lxWS",4,100}};
	};
	
	class B_Soldier_SL_F: B_Soldier_F {
		// Requires the following DLC:
		// Contact Platform
		displayName = "Yellow Plt Squad Leader";
		
		headgearClass = "tmtm_h_fastChops_grey";
		facewearClass = "G_Tactical_Clear";
		backpackClass = "FRXA_tf_rt1523g_big_Ranger_Green";
		
		linkedItems[] = {"ItemMap","ItemAndroid","TFAR_anprc152","ItemCompass","ItemWatch",""};
	};
	
	class B_Officer_F: B_Soldier_SL_F {
		// Requires the following DLC:
		// Contact Platform
		displayName = "Yellot Plt Commander";
		
		headgearClass = "H_Beret_02";
		facewearClass = "G_Aviator";
		
		linkedItems[] = {"ItemMap","ItemcTab","TFAR_anprc152","ItemCompass","ItemWatch",""};
	};
	
	class B_Medic_F: B_Soldier_F {
		// Requires the following DLC:
		// Contact Platform
		displayName = "Yellow Plt Medic";
		
		backpackClass = "B_Carryall_green_F";
		
		basicMedBackpack[] = {{"ACE_bloodIV_500",20},{"ACE_fieldDressing",100},{"ACE_personalAidKit",1},{"ACE_epinephrine",40},{"ACE_morphine",30}};
	};
	
	class B_Officer_Parade_F: B_Soldier_SL_F {
		// Requires the following DLC:
		// Contact Platform
		displayName = "Commander";
		
		headgearClass = "H_Beret_Colonel";
		facewearClass = "G_Aviator";
		vestClass = "V_CarrierRigKBT_01_heavy_Olive_F";
		
		linkedItems[] = {"ItemMap","ItemcTab","TFAR_anprc152","ItemCompass","ItemWatch",""};
	};
	
	class B_engineer_F: B_Soldier_F {
		// Requires the following DLC:
		// Contact Platform
		displayName = "Yellow Plt Logistics Officer";
		
		vestClass = "V_CarrierRigKBT_01_heavy_Olive_F";
		backpackClass = "B_AssaultPack_rgr";
		
		backpackItems[] = {{"ToolKit",1}};
	};
	
	class B_W_Soldier_F: B_Soldier_F {
		displayName = "Orange Plt Rifleman";
		
		vestClass = "tmtm_v_modularVestHiVis_rangerOrange";
	};
	
	class B_W_Soldier_AR_F: B_Soldier_AR_F {
		displayName = "Orange Plt Autorifleman";
		
		vestClass = "tmtm_v_modularVestHiVis_rangerOrange";
	};
	
	class B_W_Soldier_AAR_F: B_Soldier_AAR_F {
		displayName = "Orange Plt Assistant Autorifleman";
		
		vestClass = "tmtm_v_modularVestHiVis_rangerOrange";
	};
	
	class B_W_Soldier_TL_F: B_Soldier_TL_F {
		displayName = "Orange Plt Team Leader";
		
		vestClass = "tmtm_v_modularVestHiVis_rangerOrange";
	};
	
	class B_W_Soldier_SL_F: B_Soldier_SL_F {
		displayName = "Orange Plt Squad Leader";
		
		vestClass = "tmtm_v_modularVestHiVis_rangerOrange";
	};
	
	class B_W_Medic_F: B_Medic_F {
		displayName = "Orange Plt Medic";
		
		vestClass = "tmtm_v_modularVestHiVis_rangerOrange";
	};
	
	class B_W_Officer_F: B_Officer_F {
		displayName = "Orange Plt Commander";
		
		vestClass = "tmtm_v_modularVestHiVis_rangerOrange";
	};

	
	// -----------------------
	// -------- OPFOR --------
	// -----------------------

	class O_Soldier_F: base {
		// Requires the following DLC:
		// Apex
		// Western Sahara
		displayName = "Chevron Guard";

		primaryWeapon[] = {"CUP_arifle_AK19_black","","saber_light_lxWS","optic_MRCO",{"CUP_30Rnd_556x45_Tracer_Red_AK19_M",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"CUP_hgun_Phantom","","CUP_acc_CZ_M3X","",{"CUP_18Rnd_9x19_Phantom",18},{},""};
		binocular = "Binocular";

		uniformClass = "tmtm_u_combatUniformC_aor2B";
		headgearClass = "H_turban_02_mask_black_lxws";
		facewearClass = "G_Shades_Red";
		vestClass = "tmtm_v_6b45_black";
		backpackClass = "FRXA_tf_rt1523g_Black";

		linkedItems[] = {"ItemMap","ItemAndroid","TFAR_fadak","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"ACE_EntrenchingTool",1},{"ItemcTabHCam",1},{"CUP_18Rnd_9x19_Phantom",2,18},{"SmokeShell",2,1}};
		vestItems[] = {{"CUP_30Rnd_556x45_Tracer_Red_AK19_M",8,30},{"SmokeShell",4,1},{"ACE_M84",4,1},{"MiniGrenade",2,1},{"ACE_key_indp",1}};

		basicMedUniform[] = {{"ACE_fieldDressing",15},{"ACE_epinephrine",2},{"ACE_morphine",2}};
		basicMedVest[] = {{"ACE_fieldDressing",15}};
	};
	
	class O_Soldier_F_nodlc: O_Soldier_F {
		// Requires the following DLC:
		// Apex
		primaryWeapon[] = {"CUP_arifle_AK19_black","","CUP_acc_Flashlight","optic_MRCO",{"CUP_30Rnd_556x45_Tracer_Red_AK19_M",30},{},""};

		headgearClass = "tmtm_h_helmetEnhanced_black";
		facewearClass = "CUP_G_ESS_BLK_Scarf_Face_Blk";
	};
		
	class O_Officer_F: O_Soldier_F {
		// Requires the following DLC:
		// Apex
		// Western Sahara
		displayName = "OPFOR Officer";

		uniformClass = "tmtm_u_combatUniformC_aor2";
		headgearClass = "CUP_H_SLA_OfficerCap";
		facewearClass = "CUP_FR_NeckScarf3";
	};
	
	class O_Officer_F_nodlc: O_Officer_F {
		// Requires the following DLC:
		// Apex
		primaryWeapon[] = {"CUP_arifle_AK19_black","","CUP_acc_Flashlight","optic_MRCO",{"CUP_30Rnd_556x45_Tracer_Red_AK19_M",30},{},""};
	};
};