// SXP_fnc_createTasks
// Runs on the server to set up tasks

if (!isServer) exitWith {};

[
	true,
	["secure_chemical_weapons", ""],
	["Secure all the chemical weapons in the area of operations.", "Chemical Weapons"],
	objNull,
	"CREATED",
	10,
	true,
	"danger",
	true
] call BIS_fnc_taskCreate;

_taskLocations = [
	"Borg", 
	"Bruchwedel", 
	"Dormte", 
	"Jarlitz", 
	"Katzien", 
	"Neumuhle", 
	"Oetzen", 
	"Probien", 
	"Schwemlitz", 
	"Stocken", 
	"Suttorf"
];

{
	[
		true,
		[format["secure_chemical_weapons_%1", _x], "secure_chemical_weapons"],
		["Secure chemical weapons.", format["Secure %1", _x]],
		getMarkerPos format["obj_mark_%1", _x],
		"CREATED",
		10,
		true,
		"",
		true
	] call BIS_fnc_taskCreate;
} forEach _taskLocations;

[
	true,
	["detain_ulysses", ""],
	["Locate and detain the rogue Genaral Ulysses and his entourage.", "Detain General Ulysses"],
	objNull,
	"CREATED",
	9,
	true,
	"kill",
	true
] call BIS_fnc_taskCreate;

[
	true,
	["detain_schwenk", ""],
	["Locate and detain the evil Doctor Schwenkdawg.", "Detain Dr. Schwenkdawg"],
	objNull,
	"CREATED",
	8,
	true,
	"kill",
	true
] call BIS_fnc_taskCreate;
