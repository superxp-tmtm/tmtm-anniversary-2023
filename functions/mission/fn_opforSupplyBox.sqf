// OPFOR rearm box function
// Adds a holdaction to rearm boxes to allow OPFOR to resupply
// Call from the box's init field
params [
	["_box", objNull, [objNull]]
];

if (local _box) then {
	clearWeaponCargoGlobal _box;
	clearMagazineCargoGlobal _box;
	clearBackpackCargoGlobal _box;
	clearItemCargoGlobal _box;
};

if (!hasInterface) exitWith {};

[
	_box,
	"Resupply (REPLACES LOADOUT)",
	"a3\ui_f\data\igui\cfg\holdactions\holdaction_search_ca.paa",
	"a3\ui_f\data\igui\cfg\holdactions\holdaction_search_ca.paa",
	"playerSide == east && ((_this distance _target) < 5)",
	"playerSide == east && ((_caller distance _target) < 5)",
	{},
	{},
	{
		[_caller] call XPT_fnc_loadCurrentInventory;
		[_caller,_caller] call ace_medical_treatment_fnc_fullHeal;
	},
	{},
	[],
	3,
	1000,
	false,
	false,
	true
] call BIS_fnc_holdActionAdd;