// Flag pole setup script
// Adds the holdaction to teleport to the command vehicle

params ["_flag", "_vehicle", "_vehName"];

[
	_flag,
	format ["teleport to the %1", _vehName],
	"media\holdActions\holdAction_alert.paa",
	"media\holdActions\holdAction_alert.paa",
	"(_this distance _target) < 10",
	"(_caller distance _target) < 10",
	nil,
	nil,
	compile format ["_caller moveInCargo %1", vehicleVarName _vehicle],//{_caller moveInCargo (_arguments select 0)},
	nil,
	[_vehicle],
	1,
	1000, // Maximum priority
	false,
	false
] call BIS_fnc_holdActionAdd;