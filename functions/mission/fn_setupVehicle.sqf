// SXP_fnc_setupVehicle
// Handles setting up vehicles at mission start, and upon respawn

// Only run on the server
if (!isServer) exitWith {};

// Define variables
_this params [
	["_newVeh", nil, [objNull]],
	["_oldVeh", nil, [objNull]]
];

// If newVeh is nil, exit the script
if (isNil "_newVeh") exitWith {};

// Make sure that respawned vehicles are editable by zeus
{
	_x addCuratorEditableObjects [[_newVeh],true];
} forEach allCurators;

// Start our switch block, check the vehicle classname to determine what needs to be done for setup
switch (toLower (typeOf _newVeh)) do {
	// SOF Humvee
	case (toLower "CUP_B_nM1025_SOV_M2_NATO"): {
		[
			_newVeh,
			["MERDC_SV",1], 
			["hide_front_left_antenna",1,"hide_front_right_antenna",0,"hide_rear_left_antenna",1,"hide_rear_right_antenna",0,"hide_left_mirror",0,"hide_right_mirror",0,"hide_brushguard",0,"hide_blue_force_tracker",1,"hide_jerrycans",0,"hide_spare_wheel",0,"hide_spare_wheel_mount",0,"hide_door_front_left",0,"hide_door_front_right",0,"hide_door_rear_left",0,"hide_door_rear_right",0,"hide_cip",1,"hide_rear_view_camera",1,"hide_radio_small",0,"hide_radio_large",1,"hide_old_front_bumper",0,"hide_old_rear_bumper",1,"hide_rear_tarp_roll",0,"hide_rear_rack_content",0,"hide_rear_rack",0,"hide_backpacks",0]
		] call BIS_fnc_initVehicle;
		
		{
			_newVeh setObjectTextureGlobal [_x, "#(argb,8,8,3)color(1,1,0,0.1)"];
		} forEach [8,9,12,15,16];
		
		[_newVeh, 3, "ACE_Wheel", true] call ace_repair_fnc_addSpareParts;
		
		[_newVeh, "humvee"] call XPT_fnc_loadItemCargo;
	};
	case (toLower "CUP_B_nM1025_SOV_M2_NATO_T"): {
		[
			_newVeh,
			["MERDC_SV",1], 
			["hide_front_left_antenna",1,"hide_front_right_antenna",0,"hide_rear_left_antenna",1,"hide_rear_right_antenna",0,"hide_left_mirror",0,"hide_right_mirror",0,"hide_brushguard",0,"hide_blue_force_tracker",1,"hide_jerrycans",0,"hide_spare_wheel",0,"hide_spare_wheel_mount",0,"hide_door_front_left",0,"hide_door_front_right",0,"hide_door_rear_left",0,"hide_door_rear_right",0,"hide_cip",1,"hide_rear_view_camera",1,"hide_radio_small",0,"hide_radio_large",1,"hide_old_front_bumper",0,"hide_old_rear_bumper",1,"hide_rear_tarp_roll",0,"hide_rear_rack_content",0,"hide_rear_rack",0,"hide_backpacks",0]
		] call BIS_fnc_initVehicle;
		
		{
			_newVeh setObjectTextureGlobal [_x, "#(argb,8,8,3)color(1,0.5,0,0.1)"];
		} forEach [8,9,12,15,16];
		
		[_newVeh, 3, "ACE_Wheel", true] call ace_repair_fnc_addSpareParts;
		
		[_newVeh, "humvee"] call XPT_fnc_loadItemCargo;
	};
	
	// 2-seater pickup Humvee
	case (toLower "CUP_B_nM1038_NATO"): {
		[
			_newVeh,
			["MERDC_SV",1], 
			["hide_rear_left_antenna",0,"hide_rear_right_antenna",1,"hide_left_mirror",0,"hide_right_mirror",0,"hide_brushguard",0,"hide_blue_force_tracker",1,"hide_jerrycans",0,"hide_spare_wheel",0,"hide_spare_wheel_mount",0,"hide_door_front_left",0,"hide_door_front_right",0,"hide_cip",1,"hide_rear_view_camera",1,"hide_radio_small",0,"hide_radio_large",1,"hide_old_front_bumper",0,"hide_old_rear_bumper",1,"hide_rear_tarp",1,"hide_rear_tarp_holder",1,"hide_middle_tarp",0,"hide_middle_wall",0,"hide_front_roof",0,"hide_bench_rotate",0,"unhide_Deployment_1",0,"unhide_Deployment_2",0]
		] call BIS_fnc_initVehicle;
		
		{
			_newVeh setObjectTextureGlobal [_x, "#(argb,8,8,3)color(1,1,0,0.1)"];
		} forEach [8,9,12,13,14];
		
		[_newVeh, 3, "ACE_Wheel", true] call ace_repair_fnc_addSpareParts;
		
		[_newVeh, "humvee"] call XPT_fnc_loadItemCargo;
	};
	case (toLower "CUP_B_nM1038_NATO_T"): {
		[
			_newVeh,
			["MERDC_SV",1], 
			["hide_rear_left_antenna",0,"hide_rear_right_antenna",1,"hide_left_mirror",0,"hide_right_mirror",0,"hide_brushguard",0,"hide_blue_force_tracker",1,"hide_jerrycans",0,"hide_spare_wheel",0,"hide_spare_wheel_mount",0,"hide_door_front_left",0,"hide_door_front_right",0,"hide_cip",1,"hide_rear_view_camera",1,"hide_radio_small",0,"hide_radio_large",1,"hide_old_front_bumper",0,"hide_old_rear_bumper",1,"hide_rear_tarp",1,"hide_rear_tarp_holder",1,"hide_middle_tarp",0,"hide_middle_wall",0,"hide_front_roof",0,"hide_bench_rotate",0,"unhide_Deployment_1",0,"unhide_Deployment_2",0]
		] call BIS_fnc_initVehicle;
		
		{
			_newVeh setObjectTextureGlobal [_x, "#(argb,8,8,3)color(1,0.5,0,0.1)"];
		} forEach [8,9,12,13,14];
		
		[_newVeh, 3, "ACE_Wheel", true] call ace_repair_fnc_addSpareParts;
		
		[_newVeh, "humvee"] call XPT_fnc_loadItemCargo;
	};
	
	// Ambulance Humvee
	case (toLower "CUP_B_nM997_NATO"): {
		[
			_newVeh,
			["MERDC_SV",1], 
			["hide_antenna",0,"hide_antenna_tie_down",1,"hide_left_mirror",0,"hide_right_mirror",0,"hide_brushguard",0,"hide_door_front_left",0,"hide_door_front_right",0,"hide_cip",1,"hide_rear_view_camera",0,"hide_radio_small",0,"hide_radio_large",1,"hide_old_front_bumper",0,"hide_old_rear_bumper",1,"hide_symbols",0]
		] call BIS_fnc_initVehicle;
		
		{
			_newVeh setObjectTextureGlobal [_x, "#(argb,8,8,3)color(1,1,0,0.1)"];
		} forEach [8,9,12,13,14];

		[_newVeh, 3, "ACE_Wheel", true] call ace_repair_fnc_addSpareParts;
		
		[_newVeh, "humvee"] call XPT_fnc_loadItemCargo;
	};
	
	case (toLower "CUP_B_nM997_NATO_T"): {
		[
			_newVeh,
			["MERDC_SV",1], 
			["hide_antenna",0,"hide_antenna_tie_down",1,"hide_left_mirror",0,"hide_right_mirror",0,"hide_brushguard",0,"hide_door_front_left",0,"hide_door_front_right",0,"hide_cip",1,"hide_rear_view_camera",0,"hide_radio_small",0,"hide_radio_large",1,"hide_old_front_bumper",0,"hide_old_rear_bumper",1,"hide_symbols",0]
		] call BIS_fnc_initVehicle;
		
		{
			_newVeh setObjectTextureGlobal [_x, "#(argb,8,8,3)color(1,0.5,0,0.1)"];
		} forEach [8,9,12,13,14];

		[_newVeh, 3, "ACE_Wheel", true] call ace_repair_fnc_addSpareParts;
		
		[_newVeh, "humvee"] call XPT_fnc_loadItemCargo;
	};
	
	// Shelter Carrier Humvee
	case (toLower "CUP_B_nM1037sc_NATO"): {
		[
			_newVeh,
			["MERDC_SV",1], 
			["hide_rear_left_antenna",0,"hide_rear_right_antenna",0,"hide_left_mirror",0,"hide_right_mirror",0,"hide_brushguard",0,"hide_blue_force_tracker",0,"hide_door_front_left",0,"hide_door_front_right",0,"hide_cip",1,"hide_rear_view_camera",0,"hide_radio_small",0,"hide_radio_large",1,"hide_old_front_bumper",0,"hide_old_rear_bumper",1,"hide_middle_tarp",0,"hide_middle_wall",0,"hide_front_roof",0]
		] call BIS_fnc_initVehicle;

		[_newVeh, 3, "ACE_Wheel", true] call ace_repair_fnc_addSpareParts;
		
		[_newVeh, "humvee"] call XPT_fnc_loadItemCargo;
	};
	
	// T810 Repair
	case (toLower "CUP_B_T810_Repair_CZ_WDL"): {
		[_newVeh, 20] call ace_cargo_fnc_setSpace;
		[_newVeh, 19, "ACE_Wheel", true] call ace_repair_fnc_addSpareParts;
		
		[_newVeh, "humvee"] call XPT_fnc_loadItemCargo;
	};
	
	// T810 Ammo
	case (toLower "CUP_B_T810_Reammo_CZ_WDL"): {
		[_newVeh, 20] call ace_cargo_fnc_setSpace;
		[_newVeh, 3, "ACE_Wheel", true] call ace_repair_fnc_addSpareParts;
		
		[_newVeh, "ammotruck"] call XPT_fnc_loadItemCargo;
	};
};