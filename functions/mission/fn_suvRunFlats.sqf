// SXP_fnc_suvRunFlats
// HandleDamage event handler that prevents armoured SUVs from losing their tires

params ["_unit", "_selection", "_damage", "_source", "_projectile", "_hitIndex", "_instigator", "_hitPoint"];

private _tireList = [
	"hitlfwheel",
	"hitlf2wheel",
	"hitrfwheel",
	"hitrf2wheel",
	"hitlbwheel",
	"hitlmwheel",
	"hitrbwheel",
	"hitrmwheel"
];

if ((_hitPoint in _tireList) && {_damage > 0.90}) then {
	// Override damage handling so the wheel cannot completely die
	0.90;
};