// Global initialization script
// Runs on all machines at the end of setup

// Remove AT from enemies
["I_G_Soldier_LAT2_F", "init", {
	if (local (_this select 0)) then {
		(_this select 0) removeWeaponGlobal (secondaryWeapon (_this select 0));
		removeBackpackGlobal (_this select 0);
	};
}, false, [], true] call CBA_fnc_addClassEventHandler;

// Remove magnified optics from enemies
["I_G_Soldier_M_F", "init", {
	if (local (_this select 0)) then {
		(_this select 0) removePrimaryWeaponItem ((primaryWeaponItems (_this select 0)) select 2);
	};
}, false, [], true] call CBA_fnc_addClassEventHandler;

// Start all armoured SUVs with their guns stowed
["CUP_I_SUV_Armored_ION", "init", {
	_this spawn {
		if (local (_this select 0)) then {
			// Clear vehicle inventory
			clearWeaponCargoGlobal (_this select 0);
			clearMagazineCargoGlobal (_this select 0);
			clearBackpackCargoGlobal (_this select 0);
			clearItemCargoGlobal (_this select 0);
		
			private _handle = [(_this select 0),'turnIn'] execvm "\cup\wheeledvehicles\cup_wheeledvehicles_suv\scripts\gunnerTurn.sqf";
			waitUntil {scriptDone _handle};
			(_this select 0) lock true;
			// This part ensures that the SUVs will actually have ammo for the gunner when they turn out
			(_this select 0) setVariable ["CUP_M134_Ammo", 2000, true];
			(_this select 0) setVariable ["CUP_M134_Magazines", ["CUP_2000Rnd_TE1_Red_Tracer_762x51_M134_M"], true];
		};
	};
	
	// Add damage event handler
	(_this select 0) addEventHandler ["HandleDamage", SXP_fnc_suvRunFlats];
}, false, [], true] call CBA_fnc_addClassEventHandler;