// initPlayerLocal.sqf
// Executes on a client machine when they load the mission, regardless of if it's at mission start or JIP.
// _this = [player:Object, didJIP:Boolean]
params ["_player", "_jip"];

// Call the template initPlayerLocal function
_this call XPT_fnc_initPlayerLocal; // DO NOT CHANGE THIS LINE

// Add any mission specific code after this point

// Comment this line if you're looking to use a different method to set up briefings
[] execVM "scripts\briefing.sqf";

if (playerSide == east) then {
	// Event handler to stop OPFOR players from dying, but still allow them to go unconscious.
	sxp_preventDeathEH = [{
		// Don't execute if the player is dead
		if (alive player) then {
			if (player getVariable ["ace_medical_bloodVolume",6] < 5.3) then {player setVariable ["ace_medical_bloodVolume",5.3]};
			if (player getVariable ["ace_medical_woundBleeding",0] > 0.225) then {player setVariable ["ace_medical_woundBleeding", 0.225]};
			
			// Automatic CPR after five seconds
			if ((player getVariable ["ace_medical_heartRate",80] < 40) and {!(missionNamespace getVariable ["SXP_preventDeath_autoCPR", false])}) then {
				SXP_preventDeath_autoCPR = true;
				
				// Start our delayed execution
				[
					{(player getVariable ["ace_medical_heartRate",80] >= 40) or !(alive player)},
					{SXP_preventDeath_autoCPR = false;},
					nil,
					5,
					{["ace_medical_CPRSucceeded", player] call CBA_fnc_localEvent; SXP_preventDeath_autoCPR = false;} 
				] call CBA_fnc_waitUntilAndExecute;
			};
			
			// Automatic wake up after 60 seconds
			if (player getVariable ["ACE_isUnconscious", false] and {!(missionNamespace getVariable ["SXP_preventDeathWakeUp", false])}) then {
				// Player unconscious, and timer not started
				SXP_preventDeathWakeUp = true;
				// Start our delayed execution
				[
					{!(player getVariable ["ACE_isUnconscious", false]) or !(alive player)},
					{SXP_preventDeathWakeUp = false;},
					nil,
					60,
					{["ace_medical_WakeUp", player] call CBA_fnc_localEvent; SXP_preventDeathWakeUp = false;} 
				] call CBA_fnc_waitUntilAndExecute;
			};
		};
	}, 0.1] call CBA_fnc_addPerFrameHandler;
	
	// Loadout event handler in case the player doesn't own the Western Sahara DLC
	if !(1681170 in getDLCs 1) then {
		_player setVariable ["loadout", (_player getVariable ["loadout",typeOf _player]) + "_nodlc"];
	};
};

// Prevent BLUFOR from using OPFOR vehicle keys or radios
if (playerSide == west) then {
	_player addEventHandler ["Take", { 
		params ["_unit", "_container", "_item"]; 
		if (_item call TFAR_fnc_isRadio) then {[_item, tfar_radiocode_west] call TFAR_fnc_setSwRadioCode;};
		if (_item == "ACE_key_indp") then {_unit removeItem "ACE_key_indp"; hintSilent "Using enemy vehicle keys is not permitted in this mission";};
	}];
	
	{
		if ((_x select [0,5]) == "east_") then {
			_x setMarkerAlphaLocal 0;
		};
	} forEach allMapMarkers;
};

// BLUFOR helmet visor actions
private _lowerVisorAction = [
	"SXP_lowerVisor",
	"Lower Visor",
	"",
	{removeHeadgear _player; _player addHeadgear "tmtm_h_k6_ranger_visorDown";},
	{(headgear _player) == "tmtm_h_k6_ranger_visorUp";}
] call ace_interact_menu_fnc_createAction;
private _raiseVisorAction = [
	"SXP_raiseVisor",
	"Raise Visor",
	"",
	{removeHeadgear _player; _player addHeadgear "tmtm_h_k6_ranger_visorUp";},
	{(headgear _player) == "tmtm_h_k6_ranger_visorDown";}
] call ace_interact_menu_fnc_createAction;
["Man", 1, ["ACE_SelfActions","ACE_Equipment"], _lowerVisorAction, true] call ace_interact_menu_fnc_addActionToClass;
["Man", 1, ["ACE_SelfActions","ACE_Equipment"], _raiseVisorAction, true] call ace_interact_menu_fnc_addActionToClass;

// hold actions for each objective 
_taskLocations = [
	"Borg", 
	"Bruchwedel", 
	"Dormte", 
	"Jarlitz", 
	"Katzien", 
	"Neumuhle", 
	"Oetzen", 
	"Probien", 
	"Schwemlitz", 
	"Stocken", 
	"Suttorf"
];

{
	[
		missionNameSpace getVariable [format["terminal_%1", _x], objNull],
		"Secure Chemical Weapon",
		"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_connect_ca.paa",
		"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_connect_ca.paa",
		format["_this distance _target < 3 && !('secure_chemical_weapons_%1' call BIS_fnc_taskCompleted)", _x],
		format["_caller distance _target < 3 && !('secure_chemical_weapons_%1' call BIS_fnc_taskCompleted)", _x],
		{},
		{},
		{[format["secure_chemical_weapons_%1", _a0], "SUCCEEDED"] call BIS_fnc_taskSetState},
		{},
		[_x],
		10,
		0,
		false,
		false
	] call BIS_fnc_holdActionAdd;
} forEach _taskLocations;