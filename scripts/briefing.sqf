// Script to handle initial mission briefings
// General guidelines would be to include briefings for the following
// Situation, Mission, and Assets
// Briefings are listed in the opposite order that they are written below. New diaryRecords are always placed at the top of the list.
// https://community.bistudio.com/wiki/createDiaryRecord

/*
	===== TO CREATE A BASIC BRIEFING =====
	The following code will add a "basic" briefing to all units in the mission

	player createDiaryRecord ["Diary", ["Assets", "Example Mission Assets"]];
	player createDiaryRecord ["Diary", ["Mission", "Example Mission Briefing"]];
	player createDiaryRecord ["Diary", ["Situation", "Example Situation Briefing"]];


	===== TO CREATE A SIDE-SPECIFIC BRIEFING =====
	The following code will add a briefing *only* to a certain side
	In this example, a briefing will be created that is only visible to BLUFOR players
	UNLESS YOUR MISSION HAS MULTIPLE PLAYER SIDES. YOU DO NOT NEED THIS CODE.
	
	if ((side player) == west) then {
		player createDiaryRecord ["Diary", ["Mission", "BLUFOR mission notes go here"]];
	};
	
	
	===== TO CREATE A ZEUS-SPECIFIC BRIEFING =====
	The following code will add a briefing *only* to player zeus units.
	
	if (player isKindOf "VirtualCurator_F")then {
		player createDiaryRecord ["Diary", ["Zeus Notes", "Zeus notes go here"]];
	};
	
	
	===== NOTES =====
	Keep in mind that even with these if-statements, briefings will still appear in *reverse order from which they are written*
	This means if you want an extra note for a specific side that goes at the bottom of the briefing, that briefing should go at the top of this file.
*/

private _assetList = "Command Equipment:<br/>
	- 1x Humvee Shelter Carrier (Teleport Position)<br/>
	- 1x T810 Repair<br/>
	- 1x T810 Ammunition<br/><br/>
Platoon Command Equipment (per platoon):<br/>
	- 1x Humvee Ambulance (Teleport Position)<br/><br/>
Squad Equipment (per squad):<br/>
	- 1x Humvee SOV M2 (Does not respawn)<br/>
	- 2x Humvee pickup";

private _intel = "Looting is not permitted during this operation. We spent an awful lot of money on that equipment, and high command wouldn't be too happy if you lost any of it.<br/>
On a similar note, commandeering enemy vehicles is not permitted. You have perfectly good Humvees to use for transport, don't risk getting Blue-on-Blue'd because you stole an enemy car.<br/><br/>
OPFOR opposition forces can be ""downed"" by shooting them while they're up, which will knock them down without killing them. Use this opportunity to capture them if possible, you might be able to get some intel out of them!<br/>
Note: Shooting at an OPFOR player who is already down can kill them. Avoid this if you're looking to capture.<br/><br/>
There are flag poles at base that can be used to teleport to the Command Vehicle, or to either Platoon's command vehicle.";

private _mission = "Your mission today is to prevent these chemical weapons from being used. We have a list of towns where we believe the weapons to be present, so you'll want to start your searches there.<br/><br/>
Additionally, we have fairly strong intel that only General Ulysses and Doctor Schwenkdawg have the access codes to activate these weapons. Capturing them alive may help us bring down the whole operation, as well as any partners they're working with.";

private _situation = "A few weeks ago, known terrorist group ""The Chevron Conglomerate"" lead by General Ulysses and Doctor Schwenkdawg raided and secured a supply of deadly chemical weapons and their activation codes. Since then, the Tactical Maneuvers and Threat Management group has been trying to locate and recover the chemical weapons.<br/><br/>
Yesterday, we received validated reports that the chemical weapons were being deployed into the Rosche region in areas controlled by insurgent forces. High Command made the decision to deploy our forces to deactivate and secure these weapons.<br/><br/>
Additionally, one of our informants sent us the attached picture of what appears to be Ulysses' elite ""Chevron Guard"" operating in the region. These are highly trained forces that do not stray too far from their leadership, leading us to believe that General Ulysses and Doctor Schwenkdawg may be nearby as well.<br/>
<img title='PHOTO: Chevron Guard executing a civilian' src='media\chevron.paa' width='370' height='185'/><br/>
If you spot any of these operatives, be <font color='#FF0000'>extremely</font> careful. They are highly trained, and very well equipped.";

private _opforNotes = "READ THIS ON YOUR OWN. DO NOT REPEAT TO ANYONE ON BLUFOR.<br/><br/>
As the opposition team, you exist in this mission to make things interesting for the players (much like a Zeus does). To facilitate that, this mission has some special features:<br/>
- You are very hard to kill. See the ""%1"" note for more information.<br/>
- You have locations of secret ammo caches, task locations, and SUV stashes marked on your map. Ammo caches in red, task locations in green, SUV stashes in orange.<br/>
- Your armoured SUVs will all spawn locked, you will need to ACE interact to unlock them (you have keys for this). Make sure that you lock the SUVs whenever you leave them for any extended period of time.<br/>
- The SUV turrets all spawn stowed away, leave these stowed until you engage contacts for maximum AESTHETIC effect (the minigun popping out tends to spook people pretty well, and they don't know that the SUVs are armed.)<br/>
- Please leave the stashed SUVs for ""getaway"" situations. It's really hard to replace them in their buildings after the fact.<br/>
- You will have zeus access to teleport yourself around as needed. If you really need to spawn enemy contact, make sure to use Independent FIA groups, since they're set up with some loadout changes.<br/>
- Your long-range radio has BLUFOR encryption keys. Use this to listen in as required. DO NOT BROADCAST ON LONG-RANGE, BLUFOR *WILL* HEAR YOU.<br/><br/>
Generally, you shouldn't be actually killing BLUFOR players. Popping up and spooking people before running away is a great way to keep the op interesting, and keep players on their toes without slowing down the op too much.";

private _opforMedical = "READ THIS ON YOUR OWN. DO NOT REPEAT TO ANYONE ON BLUFOR.<br/><br/>
There is a custom script that runs in this mission that will help the OPFOR team to fulfill their intended role as high-mobility hard to kill elite forces. This script does the following to make you extremely hard to kill:<br/>
- Your blood cannot go below 5.3 liters, meaning that you will always have enough blood to wake up after being knocked unconscious.<br/>
- Your maximum bleed rate is capped to ensure that you will always have ""stable vitals"" to be able to wake up.<br/>
- Upon going into cardiac arrest, you will automatically be given CPR after five seconds to restore your heart rate.<br/>
- You will be guaranteed to wake up 60 seconds after being knocked unconscious no matter what, as long as you are not dead.<br/><br/>
Because of this, there's sort of an expectation that you will RP a little bit with the rest of the players. Please keep the following in mind:<br/>
- If you wake up surrounded by players, please play along and surrender. Leak a bit of information to them about where the devices (or Ulysses/Schwenkdawg) are, then eat your cyanide pill (by hitting the respawn button).<br/>
- If you hear yourself getting magdumped while dead, just hit the respawn button. One of the consequences of making it hard for you to die is that it's also hard for you to be ""executed"" while downed.
";


if (playerSide == east) then {
	player createDiaryRecord ["Diary", ["BLUFOR Assets", _assetList]];
	player createDiaryRecord ["Diary", ["BLUFOR Intel",_intel]];
	player createDiaryRecord ["Diary", ["BLUFOR Mission",_mission]];
	player createDiaryRecord ["Diary", ["BLUFOR Situation",_situation]];
	
	private _medicalBriefing = player createDiaryRecord ["Diary", ["OPFOR Medical - TOP SECRET",_opforMedical]];
	player createDiaryRecord ["Diary", ["OPFOR Notes - TOP SECRET",format [_opforNotes, createDiaryLink ["Diary", _medicalBriefing, "OFPOR Medical"]]]];
	
} else {
	player createDiaryRecord ["Diary", ["Assets", _assetList]];
	player createDiaryRecord ["Diary", ["Intel",_intel]];
	player createDiaryRecord ["Diary", ["Mission",_mission]];
	player createDiaryRecord ["Diary", ["Situation",_situation]];
};